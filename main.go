package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/httputil"
	"os"
	"os/signal"
	"path"
	"path/filepath"
	"strings"
)

type fileList []string

func (fl *fileList) String() string {
	return strings.Join(*fl, ", ")
}

func (fl *fileList) Set(arg string) error {
	*fl = append(*fl, arg)
	_, err := os.Stat(arg)
	if err != nil {
		if os.IsNotExist(err) {
			return fmt.Errorf("file %s doesn't exist", arg)
		}
		// Hm?
	}
	return nil
}

var (
	port                  = 8000
	address               = "localhost"
	dir                   = ""
	flist        fileList = []string{}
	noSymlink             = false
	allowList             = false
	dumpQueries           = false
	dumpRequests          = false
	indexFile             = ""
)

func main() {
	flag.BoolVar(&noSymlink, "no-sym", noSymlink, "Don't use symlinks for files")
	flag.BoolVar(&dumpQueries, "dump-queries", dumpQueries, "Dump query strings with logs")
	flag.BoolVar(&dumpRequests, "dump-requests", dumpRequests, "Dump the entire incoming request to stdout")
	flag.IntVar(&port, "p", port, "Port to listen on")
	flag.StringVar(&address, "a", address, "Address to bind to")
	flag.StringVar(&indexFile, "i", indexFile, "File to serve at /")
	flag.StringVar(&dir, "d", dir, "Directory to serve content from")
	flag.Var(
		&flist,
		"f",
		"Files to serve, may be passed mutliple times (invalidate if -d)",
	)
	flag.BoolVar(&allowList, "allow-list", allowList, "Allow file listing")

	flag.Parse()

	if len(dir) > 0 && len(flist) > 0 {
		fmt.Fprintln(os.Stderr, "can't use both -d and -f")
		os.Exit(1)
	}

	address := getAddress()

	if indexFile != "" {
		flist = append(flist, indexFile)
	}

	if len(dir) > 0 {
		setupDirectory(dir)
		log.Println("Serving directory", dir, "on", address)
	} else if len(flist) > 0 {
		tempDir := setupFiles(flist)
		defer cleanupTempDir(tempDir)
		log.Println("Serving passed files on", address)
	} else {
		log.Fatalln("must set either -f, -d, or -i")
	}

	var srv http.Server

	idleConnsClosed := make(chan struct{})
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt)
		<-sigint

		if err := srv.Shutdown(context.Background()); err != nil {
			log.Println("Error shutting down server", err)
		}
		close(idleConnsClosed)
	}()

	srv.Addr = address

	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		log.Fatalln("failed to start serving", err)
	}

	<-idleConnsClosed
}

func getAddress() string {
	return fmt.Sprintf("%s:%d", address, port)
}

func setupDirectory(dir string) {
	http.Handle("/", newCustomFileServer(http.Dir(dir), allowList))
}

func cleanupTempDir(dir string) {
	log.Println("Cleaning up", dir)
	if err := os.RemoveAll(dir); err != nil {
		log.Println("Failed to clean up", err)
	}
}

func setupFiles(files []string) string {
	allRel := areAllRelative(files)
	if !allRel {
		log.Println(
			"Not all paths are relative, directory names will be chomped",
		)
	}
	dir, err := os.MkdirTemp("", "simpleserver-*")
	if err != nil {
		log.Fatalln("failed to make temp dir", err)
	}
	for _, f := range files {
		log.Println("Adding", f, "to temp dir", dir)
		if err := copyFile(f, dir, allRel); err != nil {
			log.Fatalln("failed to copy", f, err)
		}
	}
	setupDirectory(dir)
	return dir
}

func copyFile(fname string, dir string, dontChomp bool) error {
	var fullName string
	if dontChomp {
		if err := ensureDir(fname, dir); err != nil {
			return err
		}
		fullName = filepath.Join(dir, fname)
	} else {
		fullName = filepath.Join(dir, filepath.Base(fname))
	}
	if noSymlink {
		return copyFileWrite(fullName, dir)
	}
	abs, err := filepath.Abs(fname)
	if err != nil {
		return err
	}
	return os.Symlink(abs, fullName)
}

func copyFileWrite(original string, tempname string) error {
	into, err := os.Create(tempname)
	if err != nil {
		return err
	}
	defer into.Close()
	f, err := os.Open(original)
	if err != nil {
		return err
	}
	defer f.Close()
	_, err = io.Copy(into, f)
	return err
}

func ensureDir(fname string, dir string) error {
	subDir := filepath.Dir(fname)
	if subDir == "." {
		return nil
	}
	newDir := filepath.Join(dir, subDir)
	return os.MkdirAll(newDir, 0700)
}

func areAllRelative(files []string) bool {
	for _, f := range files {
		if filepath.IsAbs(f) {
			return false
		}
	}
	return true
}

// TODO This is a wrapper around http.FileServer that
// repeats some of its work. I think that's fine, since
// we're not trying to run some high performance server,
// but it is kinda annoying.

func newCustomFileServer(dir http.Dir, allowList bool) http.Handler {
	return &customFileServer{
		fs:        dir,
		allowList: allowList,
		wrapped:   http.FileServer(dir),
	}
}

type customFileServer struct {
	fs        http.FileSystem
	allowList bool
	wrapped   http.Handler
}

func (nl *customFileServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	hasIndexFile := indexFile != ""

	if nl.allowList && !hasIndexFile {
		// Allow listing with no index file, just go straight to the wrapped
		// FileSystem
		nl.passToWrapped(w, r)
		return
	}

	upath := r.URL.Path

	if !strings.HasPrefix(upath, "/") {
		upath = "/" + upath
		r.URL.Path = upath
	}

	if hasIndexFile && upath == "/" {
		r.URL.Path = "/" + indexFile
		nl.passToWrapped(w, r)
		return
	} else if nl.allowList {
        // Have an index file but they're not asking for it. Since we allowList
        // just go to the wrapped FileSystem
		nl.passToWrapped(w, r)
		return
	}

    // The following logic is basically just to deny listing

	f, err := nl.fs.Open(path.Clean(upath))

	if err != nil {
		msg, code := toHTTPError(err)
		logRequest(r, code, len(msg))
		http.Error(w, msg, code)
		return
	}

	defer f.Close()

	s, err := f.Stat()
	if err != nil {
		msg, code := toHTTPError(err)
		logRequest(r, code, len(msg))
		http.Error(w, msg, code)
		return
	}

	if s.IsDir() {
		msg, code := "403 Forbidden", http.StatusForbidden
		logRequest(r, code, len(msg))
		http.Error(w, msg, code)
		return
	}

	nl.passToWrapped(w, r)
}

func logRequest(r *http.Request, code int, size int) {

    dumpedRequest := false

	if dumpRequests {
		raw, err := httputil.DumpRequest(r, false)
		if err == nil {
			fmt.Println(string(raw))
            dumpedRequest = true
		}
	}

	if !dumpedRequest && dumpQueries {
		log.Println(r.RemoteAddr, "-", r.Method, "-", r.URL.String(), "-", code, "-", size)
	} else {
		log.Println(r.RemoteAddr, "-", r.Method, "-", r.URL.Path, "-", code, "-", size)
	}
}

func (nl *customFileServer) passToWrapped(w http.ResponseWriter, r *http.Request) {
	wrapped := responseWriterWrapper{code: 200, wrapped: w, size: 0}

	nl.wrapped.ServeHTTP(&wrapped, r)
	logRequest(r, wrapped.code, wrapped.size)
}

func toHTTPError(err error) (string, int) {
	if os.IsNotExist(err) {
		return "404 page not found", http.StatusNotFound
	}
	if os.IsPermission(err) {
		return "403 Forbidden", http.StatusForbidden
	}
	return "500 Internal Server Error", http.StatusInternalServerError
}

type responseWriterWrapper struct {
	code    int
	size    int
	wrapped http.ResponseWriter
}

func (rww *responseWriterWrapper) Header() http.Header {
	return rww.wrapped.Header()
}

func (rww *responseWriterWrapper) Write(data []byte) (int, error) {
	n, err := rww.wrapped.Write(data)
	if err == nil {
		rww.size += n
	}
	return n, err
}

func (rww *responseWriterWrapper) WriteHeader(code int) {
	rww.code = code
	rww.wrapped.WriteHeader(code)
}
